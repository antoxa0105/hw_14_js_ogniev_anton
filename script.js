const vh = window.innerHeight; //получаю высоту области просмортра
$(function () {
   $(window).scroll(function () {  // scroll запускает функцию при прокрутке на странице
    if ($(this).scrollTop() > vh) { // при прокрутке вниз более высоты видимости fadeIn - плавное появление кнопки
      $("#toTop").fadeIn();
    } else {
      $("#toTop").fadeOut();
    }
  });
  $("#toTop").click(function () {
    $("body,html").animate({ scrollTop: 0 }, window); // по клику возвращает в позицию 0 для scrollTop
  });
});

$( ".btn-news" ).click(function(){ // задаем функцию при нажатиии на элемент с классом slide-toggle
    $( ".posts" ).slideToggle(1000); // плавно скрываем, или отображаем все элементы секции
  });


$('.ref-on-page a').on('click', function() {

    let href = $(this).attr('href');

    $('html, body').animate({
        scrollTop: $(href).offset().top
    }, {
        duration: 370,
        easing: "linear",
    });
    return false;
});